﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{
    //Called in the Animator
    public Animator animator;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        //Get Component to use it in the Movement Script
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // Character moves along the X/Z axes when given the input
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        input = Vector3.ClampMagnitude(input, 1f);
        input = transform.InverseTransformDirection(input);
    }

    public void Move(Vector2 direction)
    {
        //Players speed determined by how far the stick is times speed
        animator.SetFloat("Horizontal", direction.x * speed);
        animator.SetFloat("Vertical", direction.y * speed);
    }
}
